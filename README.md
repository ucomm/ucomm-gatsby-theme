# UCOMM Gatsby Theme

## Idea
This is a proof of concept project that came out of a few observations.

Sometimes, we have sites that are really too small or too infrequently updated to require an entire wordpress installation all to themselves. One example would be (excellence.uconn.edu)[https://excellence.uconn.edu] (in the (uconn-pride repo)[https://github.com/uconn/uconn-pride]). This is a static site, hosted by github. But it's very difficult for content managers or designers to update. It requires a dev to clone the site locally, maintain dependencies, make the updates, push them back to github.... Hardly efficient and very different from our current process. There are other similar sites as well.



## Use

## TODO