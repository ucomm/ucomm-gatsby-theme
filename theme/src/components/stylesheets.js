import React from 'react'
import { Helmet } from 'react-helmet'

const StyleSheets = ({ stylesheets }) => {

  // filter stylesheets according to their handle
  // at a later time, this may be part of the graphql query
  const links = stylesheets.filter(sheet => sheet.handle.includes('uconn')).map((sheet, index) => {
    return (
      <link key={index} id={`${sheet.handle}-css`} href={sheet.src} rel="stylesheet"></link>
    )
  })

  return (
    <Helmet>
      {links}
    </Helmet>
  )
}

export default StyleSheets