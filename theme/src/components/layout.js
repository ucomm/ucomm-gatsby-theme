import React from 'react'
import Default from '../templates/default'
import CSS from './css'
import StyleSheets from './stylesheets'

const Layout = ({ pageContext }) => {
  console.log(pageContext)
  return (
    <>
      <StyleSheets stylesheets={pageContext.stylesheets} />
      <CSS css={pageContext.css} />

      <Default 
        content={pageContext.content} 
        title={pageContext.title} 
      />
    </>
  )
}

export default Layout