import React from 'react'
// import styled from 'styled-components'
// import tw from 'twin.macro'

// import '../css/style.css'

// const mainStyles = tw`p-4 text-uconn-blue`

// const StyledMain = styled.main`
//   ${mainStyles}
// `

const Default = ({ content, title }) => {
  const data = {
    title,
    content,
  }

  return (
    <>
    <div>
      {/* <header>
        <h1>{data.title}</h1>
      </header> */}
      <main
        dangerouslySetInnerHTML={{__html: data.content}}
      >
      </main>
    </div>
    </>
  )
}

export default Default