exports.createPages = ({ graphql, actions, reporter }, themeOptions) => {
  
  const wpQuery = `
    query WPQuery {
      ${themeOptions.wpSourceQuery}
    }
  `

  return graphql(wpQuery).then(result => {

    const pageData = result.data.wpSource.page

    actions.createPage({
      path: '/',
      component: require.resolve('./src/components/layout.js'),
      context: {
        title: pageData.title,
        content: pageData.beaverBuilderContent.html,
        css: pageData.beaverBuilderContent.css,
        stylesheets: pageData.enqueuedStylesheets.nodes
      }
    })
  }).catch(err => console.log(err))
}